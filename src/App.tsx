import SearchIcon from "@mui/icons-material/Search";
import { Box, Button, CircularProgress, IconButton, InputBase, Link, Paper } from "@mui/material";
import List from "@mui/material/List";
import axios from "axios";
import React, { useState } from "react";
import { useQuery } from "react-query";
import { graphqlRequestsClient } from ".";
import "./App.css";
import MovieResult from "./components/MovieResult";
import { FetchPopularQuery, SearchMoviesQuery, useFetchPopularQuery, useSearchMoviesQuery } from "./generated/graphql";

function App() {
  const [inputValue, setInputValue] = useState("");
  const [searchQuery, setSearchQuery] = useState("");
  const [selectedMovie, setSelectedMovie] = useState<any>();
  const [relatedMovies, setRelatedMovies] = useState<any[] | null>();

  async function fetchWikiData(movieTitle: string) {
    const search = await axios.get<any[]>(
      `https://en.wikipedia.org/w/api.php?action=opensearch&search=${movieTitle}&origin=*`
    );
    const pageTitle = search.data[1][0];

    if (pageTitle) {
      const extractResults = await axios.get<any>(
        `https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exlimit=max&explaintext&exintro&titles=${pageTitle}&origin=*`
      );
      const pages = extractResults.data.query?.pages;

      if (pages) {
        const extract = pages[Object.keys(pages)[0]].extract as string;

        if (extract) return { pageTitle, extract };
      }
    }

    throw new Error("Wiki page not found");
  }

  const popularMovies = useFetchPopularQuery<FetchPopularQuery, Error>(graphqlRequestsClient, undefined, {
    enabled: !searchQuery,
    refetchOnWindowFocus: false,
    retry: false,
  });

  const searchResults = useSearchMoviesQuery<SearchMoviesQuery, Error>(
    graphqlRequestsClient,
    { query: searchQuery },
    {
      enabled: !!searchQuery,
      refetchOnWindowFocus: false,
      retry: false,
    }
  );

  const wikiData = useQuery(["wikiData", selectedMovie?.name as string], ({ queryKey }) => fetchWikiData(queryKey[1]), {
    enabled: !!selectedMovie,
    refetchOnWindowFocus: false,
    retry: false,
  });

  const handleSearchSubmit = (e: any) => {
    e.preventDefault();
    setSearchQuery(inputValue);
    setRelatedMovies(null);
  };

  const handleRelatedMoviesClick = () => {
    if (relatedMovies) setRelatedMovies(null);
    else setRelatedMovies(selectedMovie.similar);
  };

  const Loading = <CircularProgress />;
  const ErrorMessage = <div>Error while loading results!</div>;

  return (
    <Box sx={{ display: "flex" }}>
      <Box sx={{ flex: "1", padding: 1 }}>
        <Paper
          component="form"
          onSubmit={handleSearchSubmit}
          sx={{ p: "2px 4px", display: "flex", alignItems: "center", outline: "1px solid gray" }}
        >
          <InputBase
            sx={{ ml: 1, flex: 1 }}
            placeholder="Search movies"
            value={inputValue}
            onChange={(e) => setInputValue(e.target.value)}
          />
          <IconButton type="submit" sx={{ p: "10px" }}>
            <SearchIcon />
          </IconButton>
        </Paper>

        <List>
          {relatedMovies
            ? relatedMovies.map((movie, i) => (
                <MovieResult
                  key={i}
                  movie={movie}
                  onClick={() => {
                    setSelectedMovie(movie);
                  }}
                />
              ))
            : searchResults.isFetching
            ? Loading
            : searchResults.isError
            ? ErrorMessage
            : searchResults.data
            ? searchResults.data?.searchMovies.map((movie, i) => (
                <MovieResult
                  key={i}
                  movie={movie}
                  onClick={() => {
                    setSelectedMovie(movie);
                  }}
                />
              ))
            : popularMovies.isFetching
            ? Loading
            : popularMovies.isError
            ? ErrorMessage
            : popularMovies.data?.movies.map((movie, i) => (
                <MovieResult
                  key={i}
                  movie={movie}
                  onClick={() => {
                    setSelectedMovie(movie);
                  }}
                />
              ))}
        </List>
      </Box>

      <Box sx={{ flex: "2", paddingX: 2 }}>
        {selectedMovie && (
          <>
            <h2>{selectedMovie.name}</h2>
            <Box sx={{ display: "flex", alignItems: "center" }}>
              {selectedMovie.socialMedia.imdb}
              <Link
                component={Button}
                underline="none"
                target="_blank"
                rel="noreferrer"
                href={selectedMovie.socialMedia.imdb}
              >
                {/*Unfortunately the api always returns null for imdb link*/}
                Open on IMDB
              </Link>
              {wikiData.data && (
                <Link
                  component={Button}
                  underline="none"
                  target="_blank"
                  rel="noreferrer"
                  href={`https://en.wikipedia.org/wiki/${wikiData.data.pageTitle}`}
                >
                  Open on Wikipedia
                </Link>
              )}
              <Button onClick={handleRelatedMoviesClick}>{relatedMovies ? "Hide" : "Show"} related movies</Button>
            </Box>
            <h4>Wikipedia extract:</h4>
            {wikiData.isFetching ? (
              Loading
            ) : wikiData.isError ? (
              <p>Cannot load Wikipedia page!</p>
            ) : (
              <div>
                <p>{wikiData.data?.extract}</p>
              </div>
            )}
          </>
        )}
      </Box>
    </Box>
  );
}

export default App;
