import { ListItem, ListItemButton, ListItemText } from "@mui/material";
import React from "react";

function MovieResult(props: any) {
  const { movie, onClick } = props;

  const genres = movie.genres.map((g: any) => g.name).join(", ");

  return (
    <ListItem disablePadding>
      <ListItemButton onClick={onClick}>
        <ListItemText primary={movie.name} secondary={`Genres: ${genres}, Score: ${movie.score}`} />
      </ListItemButton>
    </ListItem>
  );
}

export default MovieResult;
